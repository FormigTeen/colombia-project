// Import React
import React from 'react';

// Import Spectacle Core tags
import {
  Appear,
  //BlockQuote,
  //Cite,
  Deck,
  Heading,
  ListItem,
  List,
  //Quote,
  Slide,
  Text,
  Image,
} from 'spectacle';

// Import theme
import createTheme from 'spectacle/lib/themes/default';

import ImageSlides from './ImagesSlide';

// Require CSS
require('normalize.css');

const theme = createTheme(
  {
    primary: '#FCD20E',
    secondary: '#003594',
    tertiary: 'white',
    quaternary: '#CF0821',
  },
  {
    primary: 'Montserrat',
    secondary: 'Helvetica',
  }
);

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={['zoom', 'slide']}
        transitionDuration={500}
        theme={theme}
        progress={'bar'}
      >
        <Slide transition={['zoom']} bgColor="tertiary">
          <Image src='images/flagOfColombia.svg' display='block' />
          <Heading margin="50px 0 0" size={3} caps textColor="secondary">
            Colombia
          </Heading>
          <Text margin="10px 0 0" textColor="quaternary" size={7} bold>
            Dos Voces Y Un Corazón 
          </Text>
        </Slide>
        <Slide transition={['fade']} bgColor="primary">
          <Heading size={1} margin="0px 0px 20px" textColor="quaternary" caps>
            La Geografía
          </Heading>
        </Slide>
        <Slide transition={['fade']} bgColor="secondary">
          <Heading size={5} margin="0px 0px 20px" textColor="primary" caps>
            Las Regiones
          </Heading>
            <Appear>
                <Image src='images/mapaRegiones.svg' display='block' height='550px' />
            </Appear>
        </Slide>


          <Slide transition={['fade']} bgColor="secondary">
              <Heading size={5} margin="0px 0px 20px" textColor="primary" caps>
                  Los Departamentos
              </Heading>
              <Appear>
                  <Image src='images/mapaDepartamentos.svg' display='block' height='550px' />
              </Appear>
          </Slide>
          <Slide transition={['fade']} bgColor="primary">
              <Heading size={3} margin="0px 0px 20px" textColor="quaternary" caps>
                  Entonces...
              </Heading>
              <Heading size={5} margin="0px 0px 20px" textColor="quaternary" caps>
                  ¿Pa donde vamos?
              </Heading>
          </Slide>
          <Slide transition={['fade']} bgColor="primary">
              <Heading size={5} margin="0px 0px 20px" textColor="secondary" caps>
                  Se va para Barranquilla
              </Heading>
          </Slide>
          <Slide transition={['fade']} bgColor="secondary">
              <Heading size={3} margin="0px 0px 30px" textColor="tertiary" caps>
                  Barranquilla
              </Heading>
              <Heading size={6} margin="0px 0px 20px" textColor="primary" caps>
                  Departamento de Atlántico
              </Heading>
          </Slide>
          <Slide>
              <ImageSlides title="Barranquilla" images={[
                  {
                      'title': 'Carnaval de Barranquilla',
                      'image': 'images/barranquilla/carnaval.jpg'
                  },
                  {
                      'title': 'Puerto de Barranquilla',
                      'image': 'images/barranquilla/puerto.jpg'
                  },
                  {
                      'title': 'Bocas de Ceniza',
                      'image': 'images/barranquilla/ceniza.jpg'
                  },
              ]}/>
          </Slide>


          <Slide transition={['fade']} bgColor="primary">
              <Heading size={3} margin="0px 0px 20px" textColor="quaternary" caps>
                  Calmate!
              </Heading>
              <Heading size={5} margin="0px 0px 20px" textColor="quaternary" caps>
                  Oiga, Mire y Vea
              </Heading>
          </Slide>
          <Slide transition={['fade']} bgColor="primary">
              <Heading size={5} margin="0px 0px 20px" textColor="secondary" caps>
                  A Cali para que vea ;)
              </Heading>
          </Slide>
          <Slide transition={['fade']} bgColor="secondary">
              <Heading size={3} margin="0px 0px 30px" textColor="tertiary" caps>
                  Cali
              </Heading>
              <Heading size={6} margin="0px 0px 20px" textColor="primary" caps>
                  Departamento de Valle del Cauca
              </Heading>
          </Slide>
          <Slide>
              <ImageSlides title="Cali" images={[
                  {
                      'title': 'Monumento a Cristo Rey',
                      'image': 'images/cali/cristo.jpg'
                  },
                  {
                      'title': 'Festivales y Salsa',
                      'image': 'images/cali/salsa.jpg'
                  },
                  {
                      'title': 'Iglesia de San Antonio',
                      'image': 'images/cali/iglesia.jpg'
                  }
              ]}/>
          </Slide>


          <Slide transition={['fade']} bgColor="primary">
              <Heading size={3} margin="0px 0px 20px" textColor="quaternary" caps>
                  Espera...
              </Heading>
              <Heading size={5} margin="0px 0px 20px" textColor="quaternary" caps>
                  estoy en Andes, ¿y ahora?
              </Heading>
          </Slide>
          <Slide transition={['fade']} bgColor="primary">
              <Heading size={5} margin="0px 0px 20px" textColor="secondary" caps>
                  Ven a Medellín!
              </Heading>
          </Slide>
          <Slide transition={['fade']} bgColor="secondary">
              <Heading size={3} margin="0px 0px 30px" textColor="tertiary" caps>
                  Medellín
              </Heading>
              <Heading size={6} margin="0px 0px 20px" textColor="primary" caps>
                  Departamento de Antioquia
              </Heading>
          </Slide>
          <Slide>
              <ImageSlides title="Medellín" images={[
                  {
                      'title': 'Metro de Medellín',
                      'image': 'images/medellin/metro.jpg'
                  },
                  {
                      'title': 'Museo de Antioquia',
                      'image': 'images/medellin/museo.jpg'
                  },
                  {
                      'title': 'Parque Explora',
                      'image': 'images/mdellin/parque.jpg'
                  }
              ]}/>
          </Slide>


          <Slide transition={['fade']} bgColor="primary">
              <Heading size={3} margin="0px 0px 20px" textColor="quaternary" caps>
                  En el centro,
              </Heading>
              <Heading size={5} margin="0px 0px 20px" textColor="quaternary" caps>
                  pero, ¿cuánto costó?
              </Heading>
          </Slide>
          <Slide transition={['fade']} bgColor="primary">
              <Heading size={5} margin="0px 0px 20px" textColor="secondary" caps>
                  Bogotá
              </Heading>
          </Slide>
          <Slide transition={['fade']} bgColor="secondary">
              <Heading size={3} margin="0px 0px 30px" textColor="tertiary" caps>
                  Bogotá
              </Heading>
              <Heading size={6} margin="0px 0px 20px" textColor="primary" caps>
                  Departamento de Cundinamarca y Colombia
              </Heading>
          </Slide>
          <Slide>
              <ImageSlides title="Bogotá" images={[
                  {
                      'title': 'Plaza de Bolívar',
                      'image': 'images/bogota/plaza.jpg'
                  },
                  {
                      'title': 'Museo de Oro',
                      'image': 'images/bogota/museo.JPG'
                  },
                  {
                      'title': 'Aeropuerto Internacional El Dorado',
                      'image': 'images/bogota/aeropuerto.jpg'
                  },
              ]}/>
          </Slide>


          <Slide>
              <Heading size={3} margin="0px 0px 30px" textColor="tertiary" caps>
                  Mi Intercambio
              </Heading>
              <List>
                  <Appear><ListItem>Libro Historia Mínima de Colombia - Jorge Orlando Melo ( Amazon )</ListItem></Appear>
                  <Appear><ListItem>Podcast La Historia del Mundo - Diana Uribe ( Caracol Radio )</ListItem></Appear>
                  <Appear><ListItem>Talk Ideas para dejar las Armas - Francisco Samper ( TED Talks )</ListItem></Appear>
                  <Appear><ListItem>Podcast La Historia de la Radio - Diana Uribe ( Diana Uribe )</ListItem></Appear>
                  <Appear><ListItem>Podcast Así Canta Colombia - Gabriel Muñoz ( Caracol Radio )</ListItem></Appear>
                  <Appear><ListItem>Podcast El sabor de Colombia - Aleyda Salcedo ( Caracol Radio )</ListItem></Appear>
              </List>
          </Slide>
      </Deck>
    );
  }
}
