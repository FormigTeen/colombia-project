import React, { Component } from 'react';

import {
    Heading,
    Image,
} from 'spectacle';
export default class ImagesSlide extends Component {

    constructor(props) {
       super(props);
       this.state = {
           index: 0
       };
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            5000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            index: (this.state.index + 1) % this.props.images.length
        });
    }

    render() {
        return (
            <div>
                <Heading size={5} margin="0px 0px 30px" textColor="quaternary" caps>
                    {this.props.title}
                </Heading>
                <Heading size={6} margin="0px 0px 20px" textColor="secondary">
                    {this.props.images[this.state.index].title}
                </Heading>
                <Image src={this.props.images[this.state.index].image} display='block' height='400px' />
            </div>
        );
    }

}